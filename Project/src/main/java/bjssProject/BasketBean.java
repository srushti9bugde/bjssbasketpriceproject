/**
 * 
 */
package bjssProject;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Srushti Bugde
 *
 */
public class BasketBean {
	
 private List<String> goods; //Items present in basket
 private double subtotal; //subtotal of basket
 private double total;  //total of basket
 private double discountValue; //discount applied
 
 public List<String> getGoods() {
	return goods;
 }

 public void setGoods(List<String> goods) {
 	this.goods = goods;
 }

public double getSubtotal() {
	return subtotal;
}

public void setSubtotal(double subtotal) {
	this.subtotal = subtotal;
}

public double getTotal() {
	return total;
}

public void setTotal(double total) {
	this.total = total;
}

public double getDiscountValue() {
	return discountValue;
}

public void setDiscountValue(double discountValue) {
	this.discountValue = discountValue;
}
}
