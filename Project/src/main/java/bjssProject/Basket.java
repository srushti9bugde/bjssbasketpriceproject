package bjssProject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Basket {
	/*
	 * Main method
	 */
	public static void main(String args[]) {		
		List<String> inputBasketList = new ArrayList<String>();
		System.out.println("Command line arguments passed like- PriceBaset item item item....");
		for(String argument : args) {
			if(null!= argument && !argument.equalsIgnoreCase("PriceBasket")) {
			inputBasketList.add(argument);
			System.out.println(argument);
			}
		}
		basketPriceEvaluation(inputBasketList);
		
	}

	public static Boolean basketPriceEvaluation(List<String> inputBasketList) {
		BasketImpl basketImpl = new BasketImpl();
		Boolean run = false;
		try {
			Float subtotal = basketImpl.subtotal(inputBasketList);
			System.out.println("Subtotal - " + subtotal);
			Float discountdata = basketImpl.getInput(inputBasketList);
			System.out.println("Discount" + discountdata * 100 + "p");
			System.out.println("Pay" + (subtotal - discountdata));
			run = true;
		} catch (IOException e) {
			System.out.println("IOException occured");
			e.printStackTrace();
		} catch (Exception e) {
			System.out.println("Exception occured in program");
			e.printStackTrace();
		}
		return run;
	}
}
