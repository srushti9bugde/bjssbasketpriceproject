package bjssProject;

import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

public class BasketImpl {
	Properties prop = new Properties();	
	BasketBean beandata = new BasketBean();
	BasketImpl() {		
		InputStream inputstream = getClass().getResourceAsStream("/priceList.properties");
		try {
			prop.load(inputstream);
		} catch (IOException e) {
			System.out.println("IOException occured");
			e.printStackTrace();
		}
	}
	
	 
	public Float getInput(List<String> inputBasketList) throws IOException {		
		beandata.setGoods(inputBasketList);
		Map<String,Integer> goodsToQuantity = new HashMap<String,Integer>();
		for(String inputlist : inputBasketList) {
			if(goodsToQuantity.containsKey(inputlist)) {
				goodsToQuantity.put(inputlist, goodsToQuantity.get(inputlist)+1 );
			} else {
				goodsToQuantity.put(inputlist, 1);
			}
		}
		
		Float discountApplied = calculatediscount(subtotal(inputBasketList),goodsToQuantity);
		return discountApplied;
	}
	
	/**
	 * This method returns overall calculated discount
	 * @param float1
	 * @param goodsToQuantity
	 * @return
	 */
	private Float calculatediscount(Float float1, Map<String, Integer> goodsToQuantity) {
		Float discount = (float) 0.0;
		if(goodsToQuantity.containsKey("Apple")) {
			discount = (float) (0.1 * goodsToQuantity.get("Apple"));
		}
		if (goodsToQuantity.containsKey("Soup") && goodsToQuantity.get("Soup") >= 2 
				&& goodsToQuantity.containsKey("Bread")) {			
				int count = (goodsToQuantity.get("Soup")/2 < goodsToQuantity.get("Bread") ? goodsToQuantity.get("Soup")/2: goodsToQuantity.get("Bread"));
				discount = discount + (Float.valueOf(prop.getProperty("Bread"))/2) * count;							
		} else {
			discount+= (float) 0.0;
		}
		
		return discount;
	}

	/**
	 * This method returns subtotal of basket
	 * @param inputBasketList
	 * @return
	 * @throws IOException
	 */
	public Float subtotal(List<String> inputBasketList) throws IOException {
		Float sum = (float) 0.0;
				
		for(String basketItem: inputBasketList) {
			//System.out.println("sum of item " + prop.getProperty(basketItem) );
			Float value = Float.valueOf(prop.getProperty(basketItem));
			sum = (float) (sum + value);
		}		
		return sum;
		
	}

}
