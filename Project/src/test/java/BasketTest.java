package java;
import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;


import org.junit.Test;

public class BasketTest {
	
	@Test
	public void testBasketData(){  
        List<String> items = new ArrayList<String>();
        items.add("Milk");
        items.add("Bread");
        items.add("Soup");
        assertEquals(true, Basket.basketPriceEvaluation(items));
    }  
	
	@Test
	public void testBasketData2(){  
        List<String> items = new ArrayList<String>();
        items.add("Soup");
        items.add("Soup");
        items.add("Bread");
        assertEquals(true, Basket.basketPriceEvaluation(items));
    }  

}
